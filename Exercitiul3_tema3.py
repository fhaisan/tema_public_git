#Scrieti un program care numara aparitiile cuvintelor intr-un text si le afiseaza la ecran pe 5 cele mai folosite cuvinte.


#Varianta 1
from collections import Counter

text = input("Please enter your text: ")

_split = text.split()
_count = len(_split)

print(_count)

Counter = Counter(_split)

most_occur = Counter.most_common(5)

print(most_occur)

#Varianta 2
text = input("Please enter your text: ")

_split = text.split()
_count = len(_split)
count = 0

print(_count)

for i in _split:
    if i == i:
        count = count + 1
        print(i)
