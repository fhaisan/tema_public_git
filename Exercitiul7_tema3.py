'''Tuxy trebuie sa cumpere echipamente pentru laborator. A primit doua dictionare, unul cu echipamentele ce trebuie cumparate si cantiatea lor, si unul cu preturile echipamentelor (pentru o singura unitate). Ar vrea sa stie care e costul total.

pret_pe_unitate = {
    'monitor': 300,
    'scaun': 250,
    'unitate_de_calcul: 1500,
    'unitate_grafica': 8000
}
necesar_pentru_laborator = {
    'scaun': 4,
    'unitate_de_calcul': 200,
    'unitate_grafica': 50
}'''



pret_pe_unitate = {
    'monitor': 300,
    'scaun': 250,
    'unitate_de_calcul' : 1500,
    'unitate_grafica': 8000,
}
necesar_pentru_laborator = {
    'scaun': 4,
    'unitate_de_calcul': 200,
    'unitate_grafica': 50
}
suma = 0

for item, count in necesar_pentru_laborator.items():
	print(item, count)
	pret = pret_pe_unitate.get(item, 0)
	print(pret)
	if not pret:
		continue
	suma = suma + pret * count
print(suma)

