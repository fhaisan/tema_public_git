'''Scrie?i un program Python care s? interschimbe dou? valori existente �n program.De exemplu pentru urm?toarele etichete

prima_valoare = 10
a_doua_valoare = 20
dup? execu?ia programului

print(prima_valoare)  # 20
print(a_doua_valoare) # 10


prima_valoare = 10
a_doua_valoare = 20'''


#Varianta 1
a_treia_variabila = prima_valoare
prima_valoare = a_doua_valoare
a_doua_valoare = a_treia_variabila

print(prima_valoare)
print(a_doua_valoare)



#Varianta 2
prima_valoare, a_doua_valoare = prima_valoare, a_doua_valoare

print(prima_valoare)
print(a_doua_valoare)
