'''Organiza?ia Interna?ional? a Avia?iei Civile propune un alfabet �n care fiec?rei litere �i este asignat un cuv�nt pentru a evita problemele �n �n?elegerea mesajelor critice.

Pentru a se p?stra un istoric al conversa?iilor s-a decis transcrierea lor conform urm?toarelor reguli:

fiecare cuv�nt este scris pe o singur? linie
literele din alfabet sunt separate de o virgul?
Avem nevoie de doua programe, unul de criptare si unul de decriptare care vor primi ca input textul.

Alfabetul ICAO este urmatorul:

ICAO = {
    'a': 'alfa', 'b': 'bravo', 'c': 'charlie', 'd': 'delta', 'e': 'echo',
    'f': 'foxtrot', 'g': 'golf', 'h': 'hotel', 'i': 'india', 'j': 'juliett',
    'k': 'kilo', 'l': 'lima', 'm': 'mike', 'n': 'november', 'o': 'oscar',
    'p': 'papa', 'q': 'quebec', 'r': 'romeo', 's': 'sierra', 't': 'tango',
    'u': 'uniform', 'v': 'victor', 'w': 'whiskey', 'x': 'x-ray', 'y': 'yankee',
    'z': 'zulu'
}'''




ICAO = {
    'a': 'alfa', 'b': 'bravo', 'c': 'charlie', 'd': 'delta', 'e': 'echo',
    'f': 'foxtrot', 'g': 'golf', 'h': 'hotel', 'i': 'india', 'j': 'juliett',
    'k': 'kilo', 'l': 'lima', 'm': 'mike', 'n': 'november', 'o': 'oscar',
    'p': 'papa', 'q': 'quebec', 'r': 'romeo', 's': 'sierra', 't': 'tango',
    'u': 'uniform', 'v': 'victor', 'w': 'whiskey', 'x': 'x-ray', 'y': 'yankee',
    'z': 'zulu'
}

text = input("Please enter your text here: ")
lista = list(text)

for i in lista:
    for key, value in ICAO.items():
        if i == key:
            print(value)

print(text)







