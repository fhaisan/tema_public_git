#Scrieti un program care va elimina dintr-un dictionar cheile care au valori duplicate Exemplu: {'a': 1, 'b': 1, 'c': 1, 'd': 2} -> {'d': 2}


a = {'a': 5, 'b': 6, 'c': 1, 'd': 2, 'e': 2}

rezultat = {}

for key, value in a.items():
    if value not in rezultat.values():
        rezultat[key] = value

print(rezultat)
