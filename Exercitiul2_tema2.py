#Se primesc dou? valori numerice (ca num?r sau ca ?ir de caractere la alegere). Scrie?i un program Python care s? verifice dac? fiecare cifr?/caracter din prima valoare se reg?se?te ?i �n a doua valoare (?i vice-versa).

valoarea1 = "abcd"
valoarea2 = "dcba"

if "a" in valoarea2:
    print("a, se afla in valoarea 2")
else:
    print("a, nu se afla in valoarea2")

if "b" in valoarea2:
    print("b, se afla in valoarea 2")
else:
    print("b, nu se afla in valoarea2")

if "c" in valoarea2:
    print("c, se afla in valoarea 2")
else:
    print("c, nu se afla in valoarea2")

if "d" in valoarea2:
    print("d, se afla in valoarea 2")
else:
    print("d, nu se afla in valoarea2")

if "a" in valoarea1:
    print("a, se afla in valoarea 1")
else:
    print("a, nu se afla in valoarea1")

if "b" in valoarea1:
    print("b, se afla in valoarea 1")
else:
    print("b, nu se afla in valoarea1")

if "c" in valoarea1:
    print("c, se afla in valoarea 1")
else:
    print("c, nu se afla in valoarea1")

if "d" in valoarea1:
    print("d, se afla in valoarea 1")
else:
    print("d, nu se afla in valoarea1")
