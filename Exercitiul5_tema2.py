'''Scrie?i un program Python care s? determine dac? �ntr-o list? exist? duplicate.

Cerin?e:

nu se poate folosi nici o func?ie built-in
problema trebuie rezolvat? �n cel pu?in 3 moduri'''


a_list = [1, 1, 2, 3, 3]
duplicates = []
for item in a_list:
    if a_list.count(item) > 1:
        duplicates.append(item)
print(duplicates)


duplicates_set = set(duplicates)
print(duplicates_set)
