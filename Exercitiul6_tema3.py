'''Tuxy vrea sa aleaga cel mai bun cercetator din tara. Pentru a face asta a organizat alegeri in fiecare laboator si a primit rezultatele locale, acum are nevoie de ajutorul tau sa afle rezultatele finale. Tuxy are un dictionar de forma:

rezultate_locale = {
'laboator iasi': {
    'Cercetator 1': 128,
    'Cercetator 2': 150
    'Cercetator 2': 94,
   ....
    }
'laboator bucuresti': {
    'Cercetator 3': 281,
    'Cercetator 4': 224
    'Cercetator 10': 412,
    ....
    }
}
El are nevoie de un dictionar care ii spune numarul total de voturi adunate de un cercetator.'''


#Varianta1

rezultate_locale = {
'laboator iasi': {
    'Cercetator 1': 128,
    'Cercetator 2': 150,
    'Cercetator 2': 94,
    },
'laboator bucuresti': {
    'Cercetator 3': 281,
    'Cercetator 4': 224,
    'Cercetator 10': 412,
    }
}


for laborator in rezultate_locale:
    suma = 0
    for valoare in rezultate_locale[laborator].values():
        suma = suma + valoare
    print(laborator, suma)



for i in rezultate_locale.items():
    print(i)
    for v in i:
     continue
    for key, value in v.items():
        print(value)

        


#Varianta2
rezultate_locale = {
'laboator iasi': {
    'Cercetator 1': 128,
    'Cercetator 2': 150,
    'Cercetator 3': 94,
    },
'laboator bucuresti': {
    'Cercetator 1': 281,
    'Cercetator 2': 224,
    'Cercetator 3': 412,
    }
}


cercetator1 = rezultate_locale.get('laboator iasi').get('Cercetator 1') + rezultate_locale.get('laboator bucuresti').get('Cercetator 1')
print("Numarul total de voturi al Cercetator 1 este ", cercetator1)

cercetator2 = rezultate_locale.get('laboator iasi').get('Cercetator 2') + rezultate_locale.get('laboator bucuresti').get('Cercetator 2')
print("Numarul total de voturi al Cercetator 2 este ", cercetator2)

cercetator3 = rezultate_locale.get('laboator iasi').get('Cercetator 3') + rezultate_locale.get('laboator bucuresti').get('Cercetator 3')
print("Numarul total de voturi al Cercetator 3 este ", cercetator3)

numar_total_de_voturi = [cercetator1 + cercetator2 + cercetator3]
print(numar_total_de_voturi)





