#Jocul FAZAN

raw_input = ("Bine ai venit. Acesta este jocul \'Fazan'. Apasa orice tasta pt a vedea regulile!")

print("")

print("1. Nu ai voie sa inchizi din prima.")

print("2. Toate cuvintele trebuie sa contina cel putin 3 litere.")

_input = ("Apasa orice tasta pentru a incepe.")

print("")


grupuri_inchis = ["nt", "ct", "ee", "rg", "rb", "gv", "rd", "lm", "nt", "ns", "rz", "mn", "rt", "ng", "lt", "nd", "rt", "ix", "rd", "rb", "lc", "xt", "mp", "rs", "ct", "rb", "ee", "rc", "rh", "rn", "nt"]


def inceput():

    global primele_2

    while True:
      cuvant_inceput = input("Scrie cuvantul cu care vrei sa incepi: ")

      if len(cuvant_inceput) >= 3 and cuvant_inceput.isalpha():

            if cuvant_inceput[len(cuvant_inceput)-2:] in grupuri_inchis:

                print("Ai inchis din prima. Nu ai voie, mai incearca odata.")

            else:

                print("Ai ales cuvantul", cuvant_inceput)

                primele_2 = cuvant_inceput[len(cuvant_inceput)-2:]

                player1()
            break
    else:
      print("Nu are minim 3 litere cuvantul sau ai scris cifre in loc de litere. Mai incearca odata.")


def rematch():

    while True:

        reincepere = input("Vrei sa reincepi meciul? \'Y' sau \'N': ")

        if reincepere.lower() == "y":

            inceput()

            break

        elif reincepere.lower() == "n":

            input("Ai ales sa iesi din joc. La revedere!")

            break

        else:

            print ("Alege dintre \'Y' si \'N'!")


def player1():

    global primele_2

    while True:

            player_1 = input("Player 1: ")

            if len(player_1) >=3 and player_1.isalpha():

                if player_1[:2] == primele_2:

                    primele_2 = player_1[len(player_1)-2:]

                    if player_1[len(player_1)-2:] in grupuri_inchis:

                        print ("Meci terminat. Playerul 1 a castigat!")

                        rematch()

                        break

                    else:

                        player2()

                        break

                else:

                    print ("Cuvantul tau nu incepe cu '%s'." %(primele_2))

            else:

                print ("Nu are minim 3 litere cuvantul sau ai scris cifre in loc de litere. Mai incearca odata.")


def player2():

    global primele_2

    while True:

            player_2 = input("Player 2: ")

            if len(player_2) >= 3 and player_2.isalpha():

                if player_2[:2] == primele_2:

                    primele_2 = player_2[len(player_2)-2:]

                    if player_2[len(player_2)-2:] in grupuri_inchis:

                        print ("Meci terminat. Playerul 2 a castigat!")

                        rematch()

                        break

                    else:

                        player1()

                        break

                else:

                    print ("Cuvantul tau nu incepe cu '%s'." %(primele_2))

            else:

                print ("Nu are minim 3 litere cuvantul sau ai scris cifre in loc de litere. Mai incearca odata.")

print(inceput())
