'''Scrie?i un program Python care s? �i ofere utilizatorului s? ghiceasc? un num?r cuprins �n intervalul 0 - 100 din maxim 7 �ncerc?ri.

Not?: Pentru moment num?rul poate s? fie predefinit �n cadrul programului. De exemplu:

secret = 67
La fiecare etap? de joc utilizatorul va trebui s? propun? un num?r. Pentru fiecare num?r primit programul va trebui s? fac? urm?toarele ac?iuni:

s? verifice dac? valoarea primit? este un num?r �n intervalul 0 - 100
�n caz contrar va afi?a pe ecran un mesaj de eroare
s? verifice etapa din joc
�n cazul �n care utilizatorul a epuizat cele 7 �ncerc?ri, jocul se va termina ?i utilizatorul va primi un mesaj corespunz?tor.
s? compare num?rul cu valoarea aleas? (cu secret):
dac? valorile sunt egale, juc?torul a c�?tigat jocul
dac? valoarea aleas? este mai mic? se va afi?a pe ecran: Num?rul este mai mare.
dac? valoarea aleas? este mai mare se va afi?a pe ecran: Num?rul este mai mic.'''


secret = 67

guessesTaken = 0


while guessesTaken < 7:
    guess = int(input("Please enter your guess: "))
    guessesTaken = guessesTaken + 1
    if guess not in range(0, 100):
        print("The number must be in range 0 - 100")
    elif guessesTaken >= 7:
        print("Sorry, no more guess attempts. You lose! The correct number was", secret)
    elif guess < secret:
         print("Your guess is too low")
    elif guess > secret:
        print("Your guess is too high")
    elif guess == secret:
        print("CORRECT! YOU WIN")
        break
    else:
        break
