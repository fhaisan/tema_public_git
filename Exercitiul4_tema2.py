'''Tuxy dore?te s? �mplementeze un nou paint pentru consol?. �n timpul dezvolt?rii proiectului s-a izbit de o problem? pe care nu o poate rezolva singur ?i a apelat la ajutorul t?u. Aplica?ia ?ine un istoric al tuturor mi?c?rilor pe care le-a f?cut utilizatorul �ntr-o list?.

Exemplu de date de intrare:

istoric = [
    'ST�NGA', 2,
    'JOS', 2,
    'DREAPTA', 5,
]
Fi?ierul de mai sus ne spune c? utilizatorul a mutat cursorul 2 c?su?e la st�nga dup? care 2 c?su?e �n jos, iar ultima ac?iune a fost s? pozi?ioneze cursorul cu 5 c?su?e �n dreapta fa?? de ultima pozi?ie. El dore?te un utilitar care s? �i spun? care este distan?a dintre punctul de origine (0, 0) ?i pozi?ia curent? a cursorului.'''



def muta(punct, directie, distanta=1):
  # sus: rand -1, coloana + 0
  # jos: rand +1, coloana + 0
  # stanga: rand + 0, coloana -1
  # dreata: rand + 0, coloana +1
  if directie == 'SUS':
    rand = distanta
    coloana = 0
  elif directie == 'JOS':
    rand = -distanta
    coloana = 0
  elif directie == 'STANGA':
    rand = 0
    coloana = -distanta
  elif directie == 'DREAPTA':
    rand = 0
    coloana = distanta
  else:
    rand = 0
    coloana = 0
  return (punct[0] + rand, punct[1] + coloana)
def main():
  punct = (0, 0)
  istoric = {
    'STANGA': 2,
    'JOS': 2,
    'DREAPTA': 5,
  }
  for directie, distanta in istoric.items():
    punct = muta(punct, directie, distanta)
    print(punct)
  cateta_1 = punct[0]
  cateta_2 = punct[1]
  ipotenuza = (cateta_1 ** 2 + cateta_2 ** 2) ** 0.5
  print(ipotenuza)
main()