'''Scrie?i un program Python care s? verifice dac? un num?r este prim.

Un num?r este prim dac? acesta are doar doi divizori (1 ?i pe el �nsu?i). O s? numim un divizor un num?r la care num?rul nostru se �mparte exact, sau cu alte cuvinte restul �mp?r?irii num?rului la divizor este 0.'''

num = int(input("Enter a number: "))

if num > 1:
    for i in range(2, num):
        if (num % i) == 0:
            print(num, "is not a prime number")
            print(i, "times", num // i, "is", num)
            break
    else:
        print(num, "is a prime number")

else:
    print(num, "is not a prime number")
